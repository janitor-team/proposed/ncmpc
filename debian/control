Source: ncmpc
Section: sound
Priority: optional
Maintainer: mpd maintainers <pkg-mpd-maintainers@lists.alioth.debian.org>
Uploaders: Sebastian Harl <tokkee@debian.org>, Geoffroy Youri Berret <kaliko@debian.org>
Build-Depends: debhelper-compat (= 13),
        meson (>= 0.47),
        libncurses-dev,
        libmpdclient-dev,
        liblirc-dev,
        pkg-config,
        libpcre2-dev,
# for doc
        python3-sphinx
Standards-Version: 4.6.2
Homepage: https://www.musicpd.org/clients/ncmpc/
Vcs-Git: https://salsa.debian.org/mpd-team/ncmpc.git
Vcs-Browser: https://salsa.debian.org/mpd-team/ncmpc
Rules-Requires-Root: no

Package: ncmpc
Architecture: any
Depends: ${shlibs:Depends}, ${misc:Depends}
Suggests: mpd, ncmpc-lyrics
Enhances: mpd
Provides: mpd-client
Description: ncurses-based audio player
 This package contains a text-mode client for MPD, the Music Player Daemon.
 Its goal is to provide a keyboard oriented and consistent interface to MPD,
 without wasting resources.
 .
 Features:
  - full Unicode and wide character support
  - music database browser, database search, media library
  - audio output configuration
  - lyrics
  - LIRC support
  - customizable key bindings and colors
  - tiny memory footprint, smaller than any other interactive MPD client

Package: ncmpc-lyrics
Architecture: all
Depends: ncmpc, python3, python3-unidecode, python3-bs4, python3-requests, ${misc:Depends}
Description: ncurses-based audio player (lyrics plugins)
 ncmpc is a text-mode client for MPD, the Music Player Daemon. Its goal is to
 provide a keyboard oriented and consistent interface to MPD, without wasting
 resources.
 .
 This package contains plugins to download lyrics.
